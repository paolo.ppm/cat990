/*
 * Cat990.ino
 *
 * This program is a simulator of the V1.3 CAT ROM version, for the Yaesu FT-990 transceiver.
 * A user with a V1.2 ROM could have the control of his/her radio with the same V1.3 functionality.
 * This program was designed for the Arduino Nano board series; I've not tried other board, you are free to do so.
 * It's requested to have an ATMega328 16 Mhz or better, with at least 2 KB RAM.
 *
 * It uses a copy of the declarations extracted from the free package Hamlib
 *
 * First created on: 25/11/2015
 * Author: Paolo Prina Mello

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.

 */

#include <Arduino.h>

#include <CustomSoftwareSerial.h>
#include "Cat990.h"

ft990_mem_u radioData;

unsigned char cmdFromPC[5];
unsigned char cmdToRadio[5];
unsigned char dataFromRadio[5];
unsigned char buffer16[16];
unsigned char buffer10[10];
unsigned char modelData[2];

bool firstRead = false;
bool oldPttByRTS = true;
bool currPttByRTS;
int val = 0;
uint32_t maxTime = 1000;
uint32_t startMillis = 0;
uint32_t currentMillis = 0;
long BaudRate = 4800;

CustomSoftwareSerial* RadioSerial;

void SendCmd();
void ByPassCmd();
//void AnswMemory();
//void AnswUpdate();
void AnswMeter();
void AnswFlags();
void StepFrequency(unsigned char s);
//uint64_t uint32_to_bcd(uint32_t usint);
//uint32_t bcd_to_ui32(uint64_t bcd);

// Initialize baud serial ports and digital I/O
void setup(){


	// Pin used for PTT
	pinMode(pttPin, OUTPUT);
	digitalWrite(pttPin, LOW);


	// Various digital pins
	pinMode(ledPin, OUTPUT);				// Led on pin 13
//	pinMode(_DEBUG_PIN1, OUTPUT);			// For serial debugging
//	pinMode(_DEBUG_PIN2, OUTPUT);			// For serial debugging
	pinMode(initReadPin, INPUT_PULLUP);		// Active low
	pinMode(lockPin, INPUT_PULLUP);			// Active low
	pinMode(useRTSPin, INPUT_PULLUP);		// Active low
	pinMode(pttRTSPin, INPUT_PULLUP);		// Active low
	pinMode(negateRTSPin, INPUT_PULLUP);	// Active low
	pinMode(spare1Pin, INPUT_PULLUP);		// Active low
	pinMode(spare2Pin, INPUT_PULLUP);		// Active low
	pinMode(baud1Pin, INPUT_PULLUP);		// Active low
	pinMode(baud2Pin, INPUT_PULLUP);		// Active low
	delay(250);

	if (digitalRead(baud1Pin) == HIGH)
		BaudRate *= 2;

	if (digitalRead(baud2Pin) == HIGH)
		BaudRate *= 4;

	// Hardware serial port: PC baud rate
	Serial.begin(BaudRate,SERIAL_8N2);

	// Software serial port: 990 baud rate
	pinMode(rxPin, INPUT);
	pinMode(txPin, OUTPUT);
	RadioSerial = new CustomSoftwareSerial(rxPin, txPin, false);	// Init object
	RadioSerial->begin(4800, CSERIAL_8N2);

	// Full Update V1.2 execute once flag
	firstRead = LOW;
	oldPttByRTS = HIGH;
	// Init memory
	for (int i = 0; i < 968; ++i) {
		radioData.arr_data[i] = 0;
	}
	FlashLed13();

}

// Parse and execute the command sent from the PC
void ParseCmdFromPC() {

	ft990_freq freq;
	uint32_t result;
	int ndx;
	ft990_clar clar;

	switch (cmdFromPC[4]) {
		case FT990_CMD_SPLIT :
			ByPassCmd();
			radioData.raw_data.flag1.bits.split = (cmdFromPC[3] == 1);
			break;
		case FT990_CMD_RECALLMEM :
			AnswMemory(cmdFromPC[3] - 1);
			break;
		case FT990_CMD_VFO2MEM :
			ByPassCmd();
			break;
		case FT990_CMD_LOCK :
			ByPassCmd();
			radioData.raw_data.flag2.bits.locked = (cmdFromPC[3] == 1);
			break;
		case FT990_CMD_SELVFOAB :
			ByPassCmd();
			radioData.raw_data.flag2.bits.vfo = (cmdFromPC[3] == 1);
			break;
		case FT990_CMD_MEM2VFO :
			ByPassCmd();
			break;
		case FT990_CMD_UP :
			ByPassCmd();
			StepFrequency(FT990_CMD_UP);
			break;
		case FT990_CMD_DOWN :
			ByPassCmd();
			StepFrequency(FT990_CMD_DOWN);
			break;
		case FT990_CMD_CLARIFIER :
			ByPassCmd();
			switch (cmdFromPC[3]) {
				case FT990_CLAR_RX_OFF :
					radioData.raw_data.current_front.status &= 0xFD;
					radioData.raw_data.current_rear.status &= 0xFD;
					break;
				case FT990_CLAR_RX_ON :
					radioData.raw_data.current_front.status |= 0x2;
					radioData.raw_data.current_rear.status |= 0x2;
					break;
				case FT990_CLAR_TX_OFF :
					radioData.raw_data.current_front.status &= 0xFE;
					radioData.raw_data.current_rear.status &= 0xFE;
					break;
				case FT990_CLAR_TX_ON :
					radioData.raw_data.current_front.status |= 0x1;
					radioData.raw_data.current_rear.status |= 0x1;
					break;
				case FT990_CLAR_CLEAR :
					radioData.raw_data.current_front.coffset[1] = 0;
					radioData.raw_data.current_front.coffset[0] = 0;
					radioData.raw_data.current_rear.coffset[1] = 0;
					radioData.raw_data.current_rear.coffset[0] = 0;
					break;
				default :
					clar.c_char[1] = cmdFromPC[1];
					clar.c_char[0] = cmdFromPC[0];
					if (cmdFromPC[2] == FT990_CLAR_TUNE_DOWN)
						clar.c_int = -clar.c_int;
					radioData.raw_data.current_front.coffset[1] = clar.c_char[1];
					radioData.raw_data.current_front.coffset[0] = clar.c_char[0];
					radioData.raw_data.current_rear.coffset[1] = clar.c_char[1];
					radioData.raw_data.current_rear.coffset[0] = clar.c_char[0];
			}
			break;
		case FT990_CMD_SETVFOA :
			ByPassCmd();
			freq.f_char[0] = cmdFromPC[0];
			freq.f_char[1] = cmdFromPC[1];
			freq.f_char[2] = cmdFromPC[2];
			freq.f_char[3] = cmdFromPC[3];
			result = bcd2bin((uint64_t)freq.f_long);
			freq.f_long = result;
			RadioFlags();
			GetFlags();
			if ((radioData.arr_data[1] & 0x20) == 0x20) {
				ndx = 36;
				if ((radioData.arr_data[0] & 0x2) != 0x0)
					ndx += 16;
				radioData.arr_data[ndx] = freq.f_char[3];
				radioData.arr_data[ndx + 1] = freq.f_char[2];
				radioData.arr_data[ndx + 2] = freq.f_char[1];
				radioData.arr_data[ndx + 3] = freq.f_char[0];
			}
			radioData.arr_data[4] = freq.f_char[3];
			radioData.arr_data[5] = freq.f_char[2];
			radioData.arr_data[6] = freq.f_char[1];
			radioData.arr_data[7] = freq.f_char[0];
			radioData.arr_data[20] = freq.f_char[3];
			radioData.arr_data[21] = freq.f_char[2];
			radioData.arr_data[22] = freq.f_char[1];
			radioData.arr_data[23] = freq.f_char[0];
			break;
		case FT990_CMD_SELOPMODE :
			ByPassCmd();
			RadioFlags();
			GetFlags();
			if ((radioData.arr_data[1] & 0x20) == 0x20) {
				ndx = 43;
				if ((radioData.arr_data[0] & 0x2) != 0x0)
					ndx += 16;
				switch (cmdFromPC[3]) {
					case FT990_OP_MODE_LSB :
						radioData.arr_data[ndx] = 0;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_USB :
						radioData.arr_data[ndx] = 1;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_CW2400 :
						radioData.arr_data[ndx] = 2;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_CW500 :
						radioData.arr_data[ndx] = 2;
						radioData.arr_data[ndx + 1] = 2;
						break;
					case FT990_OP_MODE_AM6000 :
						radioData.arr_data[ndx] = 3;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_AM2400 :
						radioData.arr_data[ndx] = 3;
						radioData.arr_data[ndx + 1] = 4;
						break;
					case FT990_OP_MODE_FM6 :
						radioData.arr_data[ndx] = 4;
						radioData.arr_data[ndx + 1] = 4;
						break;
					case FT990_OP_MODE_FM7 :
						radioData.arr_data[ndx] = 4;
						radioData.arr_data[ndx + 1] = 4;
						break;
					case FT990_OP_MODE_RTTYLSB :
						radioData.arr_data[ndx] = 5;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_RTTYUSB :
						radioData.arr_data[ndx] = 5;
						radioData.arr_data[ndx + 1] = 0x80;
						break;
					case FT990_OP_MODE_PKTLSB :
						radioData.arr_data[ndx] = 6;
						radioData.arr_data[ndx + 1] = 0;
						break;
					case FT990_OP_MODE_PKTFM :
						radioData.arr_data[ndx] = 6;
						radioData.arr_data[ndx + 1] = 0x80;
						break;
					default:
						break;
				}
			}
			break;
		case FT990_CMD_PACING :
			ByPassCmd();
			break;
		case FT990_CMD_PTT :
			if (digitalRead(useRTSPin) == HIGH)
				ByPassCmd();
			break;
		case FT990_CMD_UPDATE :
			AnswUpdate(cmdFromPC[3], cmdFromPC[0]);
			break;
		case FT990_CMD_TUNER :
			ByPassCmd();
			break;
		case FT990_CMD_START :
			ByPassCmd();
			break;
		case FT990_CMD_RPT :
			ByPassCmd();
			break;
		case FT990_CMD_VFOA2B :
			ByPassCmd();
			break;
		case FT990_CMD_BW :
			ByPassCmd();
			break;
		case FT990_CMD_MEMSCANSKIP :
			ByPassCmd();
			break;
		case FT990_CMD_STEPVFO :
			ByPassCmd();
			break;
		case FT990_CMD_RDMETER :
			ByPassCmd();
			AnswMeter();
			break;
		case FT990_CMD_DIMLEVEL :
			ByPassCmd();
			break;
		case FT990_CMD_RPTROFFSET :
			ByPassCmd();
			break;
		case FT990_CMD_RDFLAGS :
			cmdToRadio[3] = 0;
			ByPassCmd();
			AnswFlags();
			break;
		default:
			break;
	}
}



// Send command to radio
void SendCmd() {

	digitalWrite(ledPin, HIGH);

	RadioSerial->write(cmdToRadio[0]);
	RadioSerial->write(cmdToRadio[1]);
	RadioSerial->write(cmdToRadio[2]);
	RadioSerial->write(cmdToRadio[3]);
	RadioSerial->write(cmdToRadio[4]);
	RadioSerial->flush();

	digitalWrite(ledPin, LOW);
}


// Returns S/Meter
void AnswMeter() {

	while (RadioSerial->available() < 5);	//Wait 5 chars
	for (int i = 0; i < 5; i++) {
		Serial.write(static_cast<char>(RadioSerial->read()));
	}
}

// Cmd to read status flags
void RadioFlags() {

//	cmdToRadio[0] = 0x0;
	cmdToRadio[0] = Serial.available();
	cmdToRadio[1] = 0x0;
	cmdToRadio[2] = 0x0;
	cmdToRadio[3] = 0x0;
	cmdToRadio[4] = 0xFA;
	SendCmd();

}

// Read 3 flags from radio
void GetFlags() {

	while (RadioSerial->available() < 5);	//Wait 5 chars
	for (int i = 0; i < 5; i++) {
		dataFromRadio[i] = static_cast<char>(RadioSerial->read());
	}
	radioData.arr_data[0] = dataFromRadio[0];
	radioData.arr_data[1] = dataFromRadio[1];
	radioData.arr_data[2] = dataFromRadio[2];
	modelData[0]= dataFromRadio[3];
	modelData[1]= dataFromRadio[4];
}

// Returns 3 flag data
void AnswFlags() {

	RadioSerial->flush();
	GetFlags();
	Serial.write(radioData.arr_data[0]);
	Serial.write(radioData.arr_data[1]);
	Serial.write(radioData.arr_data[2]);
	Serial.write(modelData[0]);
	Serial.write(modelData[1]);
}

// Step radio frequency up/down 1 Mhz or 100 Khz
void StepFrequency(unsigned char stepSign) {
ft990_freq freq;
uint32_t freq_step;

	(cmdFromPC[3] == 0x1) ? freq_step = 100000 : freq_step = 10000;
	freq.f_char[0] = 0;
	freq.f_char[1] = radioData.arr_data[1];
	freq.f_char[2] = radioData.arr_data[2];
	freq.f_char[3] = radioData.arr_data[3];
	if (stepSign == FT990_CMD_UP) {
		freq.f_long += freq_step;
		if (freq.f_long > 3000000)		// Control max frequency limit
			freq.f_long = 10000;
	}
	else {
		freq.f_long -= freq_step;
		if (freq.f_long < 10000)		// Control min frequency limit
			freq.f_long = 3000000;
	}
	radioData.arr_data[1] = freq.f_char[1];
	radioData.arr_data[2] = freq.f_char[2];
	radioData.arr_data[3] = freq.f_char[3];

	// Convert to BCD
	uint64_t freq_bcd = bin2bcd(freq.f_long);
	cmdToRadio[0] = (freq_bcd & 0x0ff000000);
	cmdToRadio[1] = (freq_bcd & 0x0ff0000);
	cmdToRadio[2] = (freq_bcd & 0x0ff00);
	cmdToRadio[3] = (freq_bcd & 0x0ff);
	cmdToRadio[4] = 0x0A;
	SendCmd();

}

// bin2bcd returns packed bcd
uint64_t bin2bcd(uint32_t usint) {

    uint64_t shift = 16;
    uint64_t result = (usint % 10);

    while ((usint = (usint/10)) != 0) {
        result += (usint % 10) * shift;
        shift *= 16;
    }
    return result;
}

// bcd2bin returns unsigned int 32
uint32_t bcd2bin(uint64_t bcd) {

    uint64_t mask = 0x000f;
    uint64_t pwr = 1;

    uint64_t i = (bcd & mask);
    while ((bcd = (bcd >> 4)) != 0) {
        pwr *= 10;
        i += (bcd & mask) * pwr;
    }
    return (uint32_t)i;
}

// Read the whole bunch of data from the V1.2 Full update radio answer
void GetFullUpdate() {

	bool readOK = LOW;

	// Repeat until answer from radio
	while (readOK == LOW) {

		// Initialize RX buffer to empty
		//RadioSerial->flush();

		// Visual feedback - reading data started
		digitalWrite(ledPin, HIGH);

		// Send full Update V1.2 command
		cmdToRadio[4] = FT990_CMD_UPDATE;
		cmdToRadio[3] = 0;
		cmdToRadio[2] = 0;
		cmdToRadio[1] = 0;
		cmdToRadio[0] = 0;
		SendCmd();

		// Initialize timeout var
		startMillis = millis();
		int isTimeout = 0;

		// Exit for timeout or chars availables
		while (isTimeout == 0) {
			if (RadioSerial->available() > 4) {
				readOK = HIGH;
				break;
			}
			else {
				isTimeout = GetSerialTimeout();
				if (isTimeout == -1) {
					digitalWrite(ledPin, LOW);
					delay(1000);
				}
			}
		}
	}

	digitalWrite(ledPin, HIGH);

	int i = 0;

	// Local mem Start address
	int ndx = 0;

	while (RadioSerial->available() < 4);	// Flags 1, 2, 3, Memory channel
	for (i = 0; i < 4; ++i) {
		radioData.arr_data[ndx] = static_cast<char>(RadioSerial->read());
		ndx++;
	}

	// Local mem Start address
	ndx = 4;

	while (RadioSerial->available() < 16);	// Current operation (Front)
	for (i = 0; i < 16; ++i) {
		radioData.arr_data[ndx] = static_cast<char>(RadioSerial->read());
		radioData.arr_data[ndx + 16] = radioData.arr_data[ndx];		//Copy to Current operation (Rear)
		ndx++;
	}

	// Local mem Start address
	ndx = 36;

	while (RadioSerial->available() < 32);	// VFO A, B
	for (i = 0; i < 32; ++i) {
		radioData.arr_data[ndx] = static_cast<char>(RadioSerial->read());
		ndx++;
	}

	// Local mem Start address
	ndx = 68;

	int j = 0;
	for (int i = 0; i < 90; i++) {		// Read all 90 memories and encode data
		while (RadioSerial->available() < 16);
		for (j = 0; j < 16; ++j)
			buffer16[j] = static_cast<char>(RadioSerial->read());
		buffer10[0] = buffer16[0];
		(buffer16[15] == 0xFF) ? buffer10[0] |= 0x40 : buffer10[0] &= 0xBF;
		buffer10[1] = buffer16[1];
		buffer10[2] = buffer16[2];
		buffer10[3] = buffer16[3];
		buffer10[3] |= (buffer16[14] << 6);
		buffer10[3] |= (buffer16[14] & 0x80);
		buffer10[4] = buffer16[4];
		buffer10[4] |= ((buffer16[8] & 0x7) << 3);
		buffer10[4] |= (buffer16[8] & 0x80);
		buffer10[5] = buffer16[5];
		buffer10[6] = buffer16[6];
		buffer10[7] = buffer16[7];
		buffer10[7] |= (buffer16[9] << 4);
		buffer10[7] |= (buffer16[9] & 0x80);
		buffer10[8] = buffer16[10];
		buffer10[8] |= (buffer16[10] >> 4);
		buffer10[8] |= (buffer16[11] << 4);
		buffer10[8] |= (buffer16[11] & 0x80);
		buffer10[9]	= buffer16[12];
		buffer10[9] |= (buffer16[12] >> 4);
		buffer10[9] |= (buffer16[13] << 4);
		for (j = 0; j < 10; ++j) {
			radioData.arr_data[ndx] = buffer10[j];
			ndx++;
		}
	}

	// Visual feedback - reading data finished
	digitalWrite(ledPin, LOW);
}

// Send the whole received PC command to radio
void ByPassCmd() {
int i;

	for (i = 0; i < 5; i++)
		cmdToRadio[i] = cmdFromPC[i];
	SendCmd();
}

// Decode local mem
void DecodeMem(int ndx) {

	for (int i = 0; i < 16; ++i) {
		buffer16[i] = 0;
	}
	buffer16[0] = radioData.arr_data[ndx] & 0x3F;
	buffer16[1] = radioData.arr_data[ndx + 1];
	buffer16[2] = radioData.arr_data[ndx + 2];
	buffer16[3] = radioData.arr_data[ndx + 3] & 0x3F;
	buffer16[4] = radioData.arr_data[ndx + 4] & 0x0F;
	buffer16[5] = radioData.arr_data[ndx + 5];
	buffer16[6] = radioData.arr_data[ndx + 6];
	buffer16[7] = radioData.arr_data[ndx + 7] & 0x07;
	buffer16[8] = (radioData.arr_data[ndx + 4] & 0x70) >> 4;
	buffer16[8] |= radioData.arr_data[ndx + 4] >> 7;
	buffer16[9] = radioData.arr_data[ndx + 7] >> 4;
	buffer16[10] = radioData.arr_data[ndx + 8] & 0x0F;
	buffer16[11] = radioData.arr_data[ndx + 8] >> 4;
	buffer16[12] = radioData.arr_data[ndx + 9] & 0x0F;
	buffer16[13] = radioData.arr_data[ndx + 9] >> 4;
	((radioData.arr_data[ndx + 3] & 0x40) == 0x40) ? buffer16[14] = 0x1 : buffer16[14] = 0;
	buffer16[14] |= radioData.arr_data[ndx + 3] & 0x80;
	((radioData.arr_data[ndx] & 0x40) == 0x40) ? buffer16[15] = 0xFF : buffer16[15] = 0;
}


// Helper for the Update answers, get the data from local mem
void AnswMemory(unsigned char param) {

	int ndx = 68 + (10 * param);
	DecodeMem(ndx);
	for (int i = 0; i < 16; ++i) {
		Serial.write(buffer16[i]);
	}
}

// Helper for the Update answers, get the data from local mem
void SendUpdArray(int first, int num) {

	for (int i = 0; i < num; ++i) {
		Serial.write(radioData.arr_data[first + i]);
	}
}

// Answer to client: Update
void AnswUpdate(unsigned char param1, unsigned char param2) {

	switch (param1) {
		case FT990_GETUPDATE_ALL :
			SendUpdArray(0, 68);
			for (int i = 0; i < 90; ++i) {
				AnswMemory(i);
			}
			break;
		case FT990_GETUPDATE_MEM :
			SendUpdArray(3, 1);
			break;
		case FT990_GETUPDATE_CURR :
//			FlashLed13();
			SendUpdArray(4, 16);
			SendUpdArray(20, 16);
			break;
		case FT990_GETUPDATE_VFOS :
			SendUpdArray(36, 32);
			break;
		case FT990_GETUPDATE_DMEM :
			AnswMemory(param2);
			break;
		default :
			break;
	}
}


int GetSerialTimeout() {

	// Get milliseconds from first character read from PC
	currentMillis = millis();
	if ((uint32_t)(currentMillis - startMillis) < maxTime)
		return (0);
	else
		return (-1);

}

// Initialize radio
void GetFirstUpdate() {

	if (firstRead == false) {	// Read once
		RadioSerial->flush();

		// Set a know VFO in case the mode is memory or M tune
		cmdToRadio[4] = FT990_CMD_SELVFOAB;
		cmdToRadio[3] = 0;
		cmdToRadio[2] = 0;
		cmdToRadio[1] = 0;
		cmdToRadio[0] = 0;
		SendCmd();
		delay(200);

		// If second jumper is set lock frequency knob
		if (digitalRead(lockPin) == LOW) {
			cmdToRadio[4] = FT990_CMD_LOCK;
			cmdToRadio[3] = 0x1;
			cmdToRadio[2] = 0;
			cmdToRadio[1] = 0;
			cmdToRadio[0] = 0;
			SendCmd();
			radioData.raw_data.flag2.bits.locked = 1;
		}

		// Get full Update like V1.2
//		cmdToRadio[4] = FT990_CMD_UPDATE;
//		cmdToRadio[3] = 0;
//		cmdToRadio[2] = 0;
//		cmdToRadio[1] = 0;
//		cmdToRadio[0] = 0;
//		SendCmd();
		firstRead = true;
		GetFullUpdate();
	}

}

// Flash three times the len on port 13, just to show that the program was started
void FlashLed13() {

	digitalWrite(ledPin, HIGH);
	delay(250);
	digitalWrite(ledPin, LOW);
	delay(100);
	digitalWrite(ledPin, HIGH);
	delay(250);
	digitalWrite(ledPin, LOW);
	delay(100);
	digitalWrite(ledPin, HIGH);
	delay(250);
	digitalWrite(ledPin, LOW);

}



void loop() {

	int isTimeout = 0;

	// Detect if first read pushbutton
	if ((Serial.available() == 0) && (digitalRead(initReadPin) == LOW) && (firstRead == false))
		GetFirstUpdate();

	// No character sent by PC
	if (Serial.available() == 0) {
		startMillis = millis();
		isTimeout = 0;
	}

	// 1 to 4 characters in the PC entry buffer, check timeout
	if ((Serial.available() > 0) && (Serial.available() < 5)) {
		isTimeout = GetSerialTimeout();
		if (isTimeout == -1)
			Serial.readBytes(cmdFromPC, Serial.available());
	}

	// Use RTS from serial port, negate output xoring with negateRTSPin
	if (digitalRead(useRTSPin) == LOW)
//		currPttByRTS = digitalRead(pttRTSPin);
		digitalWrite(pttPin, digitalRead(pttRTSPin)^digitalRead(negateRTSPin));
//		if (currPttByRTS != oldPttByRTS) {
//			digitalWrite(pttPin, !currPttByRTS^digitalRead(negateRTSPin));
//			oldPttByRTS = currPttByRTS;
//		}
//	}


	// With 5 chars process the PC command
	if (Serial.available() > 4) {
		GetFirstUpdate();
		Serial.readBytes(cmdFromPC, 5);
		delay(10);
		ParseCmdFromPC();
	}
}
