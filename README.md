# CAT990

  This program is a simulator of the V1.3 CAT ROM version, for the Yaesu FT-990 transceiver.
  A user with a V1.2 ROM could have the control of his/her radio with the same V1.3 functionality.
  This program was designed for the Arduino Nano board series; I've not tried other board, you are free to do so.
  It's requested to have an ATMega328 16 Mhz or better, with at least 2 KB RAM.
 
  It uses a copy of the declarations extracted from the free package Hamlib
 
  First created on: 25/11/2015
  Author: Paolo Prina Mello

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

