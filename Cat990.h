/*
 * Cat990.h
 *
 *  Created on: 25/11/2015
 *      Author: Paolo
 */

/*
* This header it's a modified copy of the original ft990.h hamlib.
*
* hamlib - (C) Stephane Fillod 2002, 2003 (fillods at users.sourceforge.net)
*
* ft990.h - (C) Berndt Josef Wulf (wulf at ping.net.au)
*
* This shared library provides an API for communicating
* via serial interface to an FT-990 using the "CAT" interface
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

* Modified ft990.h hamlib header for CAT_990.ino - V1.2 to V1.3 utility
* First release: 25/11/2015
* Author (C): Paolo Prina Mello (paolo dot ppm at gmail dot com)
*
*/

#ifndef CAT990_H_
#define CAT990_H_

#define txPin 				3
#define rxPin 				2
#define initReadPin			4		// header 1
#define lockPin				5		// header 2
#define useRTSPin			6		// header 3
#define negateRTSPin		7		// header 4
#define spare1Pin			8		// header 5
#define spare2Pin			9		// header 6
#define baud1Pin			10		// header 7
#define baud2Pin			11		// header 8
#define pttPin 				A0
#define pttRTSPin			A1
#define ledPin 				13

//  baud  1 2
//  4800  0 0
//  9600  1 0
// 19200  0 1
// 38400  1 1
// all is 8,N,2

#define _DEBUG_PIN1 		10
#define _DEBUG_PIN2 		11

// FT990 native commands
typedef enum ft990_native_cmd_e {
  FT990_NATIVE_SPLIT_OFF = 0,
  FT990_NATIVE_SPLIT_ON,
  FT990_NATIVE_RECALL_MEM,
  FT990_NATIVE_VFO_TO_MEM,
  FT990_NATIVE_LOCK_OFF,
  FT990_NATIVE_LOCK_ON,
  FT990_NATIVE_VFO_A,
  FT990_NATIVE_VFO_B,
  FT990_NATIVE_MEM_TO_VFO,
  FT990_NATIVE_VFO_STEP_UP,
  FT990_NATIVE_VFO_STEP_UP_FAST,
  FT990_NATIVE_VFO_STEP_DOWN,
  FT990_NATIVE_VFO_STEP_DOWN_FAST,
  FT990_NATIVE_RX_CLARIFIER_OFF,
  FT990_NATIVE_RX_CLARIFIER_ON,
  FT990_NATIVE_TX_CLARIFIER_OFF,
  FT990_NATIVE_TX_CLARIFIER_ON,
  FT990_NATIVE_CLEAR_CLARIFIER_OFFSET,
  FT990_NATIVE_CLARIFIER_OPS,
  FT990_NATIVE_FREQ_SET,
  FT990_NATIVE_MODE_SET_LSB,
  FT990_NATIVE_MODE_SET_USB,
  FT990_NATIVE_MODE_SET_CW_W,
  FT990_NATIVE_MODE_SET_CW_N,
  FT990_NATIVE_MODE_SET_AM_W,
  FT990_NATIVE_MODE_SET_AM_N,
  FT990_NATIVE_MODE_SET_FM,
  FT990_NATIVE_MODE_SET_RTTY_LSB,
  FT990_NATIVE_MODE_SET_RTTY_USB,
  FT990_NATIVE_MODE_SET_PKT_LSB,
  FT990_NATIVE_MODE_SET_PKT_FM,
  FT990_NATIVE_PACING,
  FT990_NATIVE_PTT_OFF,
  FT990_NATIVE_PTT_ON,
  FT990_NATIVE_UPDATE_ALL_DATA,
  FT990_NATIVE_UPDATE_MEM_CHNL,
  FT990_NATIVE_UPDATE_OP_DATA,
  FT990_NATIVE_UPDATE_VFO_DATA,
  FT990_NATIVE_UPDATE_MEM_CHNL_DATA,
  FT990_NATIVE_TUNER_OFF,
  FT990_NATIVE_TUNER_ON,
  FT990_NATIVE_TUNER_START,
  FT990_NATIVE_RPTR_SHIFT_NONE,
  FT990_NATIVE_RPTR_SHIFT_MINUS,
  FT990_NATIVE_RPTR_SHIFT_PLUS,
  FT990_NATIVE_VFO_TO_VFO,
  FT990_NATIVE_BANDWIDTH,
  FT990_NATIVE_OP_FREQ_STEP_UP,
  FT990_NATIVE_OP_FREQ_STEP_DOWN,
  FT990_NATIVE_READ_METER,
  FT990_NATIVE_DIM_LEVEL,
  FT990_NATIVE_RPTR_OFFSET,
  FT990_NATIVE_READ_FLAGS,
  FT990_NATIVE_SIZE
} ft990_native_cmd_t;

//  OpCode Declarations
#define FT990_CMD_SPLIT       		0x01
#define FT990_CMD_RECALLMEM   		0x02
#define FT990_CMD_VFO2MEM     		0x03
#define FT990_CMD_LOCK        		0x04
#define FT990_CMD_SELVFOAB    		0x05
#define FT990_CMD_MEM2VFO     		0x06
#define FT990_CMD_UP          		0x07
#define FT990_CMD_DOWN        		0x08
#define FT990_CMD_CLARIFIER   		0x09
#define FT990_CMD_SETVFOA     		0x0A
#define FT990_CMD_SELOPMODE   		0x0C
#define FT990_CMD_PACING      		0x0E
#define FT990_CMD_PTT         		0x0F
#define FT990_CMD_UPDATE      		0x10
#define FT990_CMD_TUNER       		0x81
#define FT990_CMD_START       		0x82
#define FT990_CMD_RPT         		0x84
#define FT990_CMD_VFOA2B      		0x85
#define FT990_CMD_BW          		0x8C
#define FT990_CMD_MEMSCANSKIP 		0x8D
#define FT990_CMD_STEPVFO     		0x8E
#define FT990_CMD_RDMETER     		0xF7
#define FT990_CMD_DIMLEVEL    		0xF8
#define FT990_CMD_RPTROFFSET  		0xF9
#define FT990_CMD_RDFLAGS     		0xFA

// Bandwidth Filter
#define FT990_BW_F2400        		0x00
#define FT990_BW_F2000        		0x01
#define FT990_BW_F500         		0x02
#define FT990_BW_F250         		0x03
#define FT990_BW_F6000        		0x04
#define FT990_BW_FMPKTRTTY    		0x80

// Operating Mode Status
#define FT990_MODE_LSB        		0x00
#define FT990_MODE_USB        		0x01
#define FT990_MODE_CW         		0x02
#define FT990_MODE_AM         		0x03
#define FT990_MODE_FM         		0x04
#define FT990_MODE_RTTY       		0x05
#define FT990_MODE_PKT        		0x06

// Operation Mode Selection
#define FT990_OP_MODE_LSB     		0x00
#define FT990_OP_MODE_USB     		0x01
#define FT990_OP_MODE_CW2400  		0x02
#define FT990_OP_MODE_CW500   		0x03
#define FT990_OP_MODE_AM6000  		0x04
#define FT990_OP_MODE_AM2400  		0x05
#define FT990_OP_MODE_FM6      		0x06
#define FT990_OP_MODE_FM7      		0x07
#define FT990_OP_MODE_RTTYLSB 		0x08
#define FT990_OP_MODE_RTTYUSB 		0x09
#define FT990_OP_MODE_PKTLSB  		0x0A
#define FT990_OP_MODE_PKTFM   		0x0B

// Clarifier Operation
#define FT990_CLAR_TX_EN      		0x01
#define FT990_CLAR_RX_EN      		0x02
#define FT990_CLAR_RX_OFF     		0x00
#define FT990_CLAR_RX_ON      		0x01
#define FT990_CLAR_TX_OFF     		0x80
#define FT990_CLAR_TX_ON      		0x81
#define FT990_CLAR_CLEAR      		0xFF
#define FT990_CLAR_TUNE_UP    		0x00
#define FT990_CLAR_TUNE_DOWN  		0xFF

// Repeater Shift Enable
#define FT990_RPT_POS_EN      		0x04
#define FT990_RPT_NEG_EN      		0x08
#define FT990_RPT_MASK        		0x0C

// Status Flag 1 Masks
#define FT990_SF_SPLIT              0x01
#define FT990_SF_VFOB               0x02
#define FT990_SF_FAST               0x04
#define FT990_SF_CAT                0x08
#define FT990_SF_TUNING             0x10
#define FT990_SF_KEY_ENTRY          0x20
#define FT990_SF_MEM_EMPTY          0x40
#define FT990_SF_XMIT               0x80

// Status Flag 2 Masks
#define FT990_SF_MEM_SCAN_PAUSE     0x01
#define FT990_SF_MEM_CHECK          0x02
#define FT990_SF_MEM_SCAN           0x04
#define FT990_SF_LOCKED             0x08
#define FT990_SF_MTUNE              0x10
#define FT990_SF_VFO                0x20
#define FT990_SF_MEM                0x40
#define FT990_SF_GEN                0x80

// Status Flag 3 Masks
#define FT990_SF_PTT                0x01
#define FT990_SF_TX_INHIBIT         0x02
#define FT990_SF_KEY_TIMER          0x04
#define FT990_SF_MEM_TIMER          0x08
#define FT990_SF_PTT_INHIBIT        0x10
#define FT990_SF_XMIT_MON           0x20
#define FT990_SF_TUNER_ON           0x40
#define FT990_SF_SIDETONE           0x80

#define FT990_EMPTY_MEM             0x80

#define FT990_AMFILTER2400          0x80

// Get update
#define FT990_GETUPDATE_ALL			0x0			//1508 bytes	Full update
#define FT990_GETUPDATE_MEM 		0x1			//1 byte		Actual memory number
#define FT990_GETUPDATE_CURR		0x2			//16 bytes		Current Operation
#define FT990_GETUPDATE_VFOS		0x3			//16+16 bytes	VFO A & B
#define FT990_GETUPDATE_DMEM		0x4			//16 bytes		Memory data


// Flags Byte 1
typedef struct _ft990_flags1_t {
  unsigned split:       			1;
  unsigned vfob:        			1;
  unsigned fast:        			1;
  unsigned cat:         			1;
  unsigned tuning:      			1;
  unsigned keyentry:    			1;
  unsigned memempty:    			1;
  unsigned xmit:        			1;
} ft990_flags1_t;

// Flags Byte 2
typedef struct _ft990_flags2_t {
  unsigned memscanpause:			1;
  unsigned memcheck:    			1;
  unsigned memscan:     			1;
  unsigned locked:      			1;
  unsigned mtune:       			1;
  unsigned vfo:         			1;
  unsigned mem:         			1;
  unsigned gen:         			1;
} ft990_flags2_t;

// Flags Byte 3
typedef struct _ft990_status3_t {
  unsigned ptt:         			1;
  unsigned txinhibit:   			1;
  unsigned keytimer:    			1;
  unsigned memtimer:    			1;
  unsigned pttinhibit:  			1;
  unsigned xmitmon:     			1;
  unsigned tuneron:     			1;
  unsigned sidetone:    			1;
} ft990_flags3_t;

typedef union _ft990_flags1_u {
  ft990_flags1_t bits;
  unsigned char byte;
} ft990_flags1_u;

typedef union _ft990_flags2_u {
  ft990_flags2_t bits;
  unsigned char byte;
} ft990_flags2_u;

typedef union _ft990_flags3_u {
  ft990_flags3_t bits;
  unsigned char byte;
} ft990_flags3_u;

typedef struct _ft990_status_data_t {
  ft990_flags1_u flags1;
  ft990_flags2_u flags2;
  ft990_flags3_u flags3;
  unsigned char id1;
  unsigned char id2;
} ft990_status_data_t;

typedef struct _ft990_meter_data_t {
  unsigned char mdata1;
  unsigned char mdata2;
  unsigned char mdata3;
  unsigned char mdata4;
  unsigned char id1;
} ft990_meter_data_t;

typedef struct _ft990_op_data_t {
  unsigned char bpf;
  unsigned char basefreq[3];
  unsigned char status;
  unsigned char coffset[2];
  unsigned char mode;
  unsigned char filter;
  unsigned char lastssbfilter;
  unsigned char lastcwfilter;
  unsigned char lastrttyfilter;
  unsigned char lastpktfilter;
  unsigned char lastclariferstate;
  unsigned char skipscanamfilter;
  unsigned char amfm100;
} ft990_op_data_t;

typedef struct _ft990_op_data_compact_t {
  unsigned char c_mixed1;
  unsigned char c_basefreq[3];
  unsigned char c_mixed2;
  unsigned char c_coffset[2];
  unsigned char c_mixed3;
  unsigned char c_mixed4;
  unsigned char c_mixed5;
} ft990_op_data_compact_t;

// Update Data Structure
typedef struct _ft990_update_data_t {
  unsigned char flag1;
  unsigned char flag2;
  unsigned char flag3;
  unsigned char channelnumber;
  ft990_op_data_t current_front;
  ft990_op_data_t current_rear;
  ft990_op_data_t vfoa;
  ft990_op_data_t vfob;
  ft990_op_data_t channel[90];
} ft990_update_data_t;

// Command Structure
typedef struct _ft990_command_t {
  unsigned char data[4];
  unsigned char opcode;
} ft990_command_t;

// Full data compact size
typedef struct _ft990_data_t {
	ft990_flags1_u flag1;
	ft990_flags2_u flag2;
	ft990_flags3_u flag3;
	unsigned char channelnumber;
	ft990_op_data_t current_front;
	ft990_op_data_t current_rear;
	ft990_op_data_t vfoa;
	ft990_op_data_t vfob;
	ft990_op_data_compact_t channel[90];
} ft990_data_t;

typedef union _ft990_mem_u {
	ft990_data_t raw_data;
	unsigned char arr_data[sizeof(ft990_data_t)];
} ft990_mem_u;

typedef union _ft990_freq_u {
	int32_t f_long;
	unsigned char f_char[4];
} ft990_freq;

typedef union _ft_990_clar_u {
	int16_t c_int;
	unsigned char c_char[2];
} ft990_clar;

#endif /* CAT990_H_ */

/*
ft990_mem_u:
0	Flag1
1	Flag2
2	Flag3
3	Mem CH
4	bpf				Front
5	basefreq[3]
8	status
9	coffset[2]
11	mode
12	filter
13	lastssbfilter
14	lastcwfilter
15	lastrttyfilter
16	lastpktfilter
17	lastclariferstate
18	skipscanamfilter
19	amfm100
20	bpf				Rear
21	basefreq[3]
24	status
25	coffset[2]
27	mode
28	filter
29	lastssbfilter
30	lastcwfilter
31	lastrttyfilter
32	lastpktfilter
33	lastclariferstate
34	skipscanamfilter
35	amfm100
36	bpf				VFO A
37	basefreq[3]
40	status
41	coffset[2]
43	mode
44	filter
45	lastssbfilter
46	lastcwfilter
47	lastrttyfilter
48	lastpktfilter
49	lastclariferstate
50	skipscanamfilter
51	amfm100
52	bpf				VFO B
53	basefreq[3]
56	status
57	coffset[2]
59	mode
60	filter
61	lastssbfilter
62	lastcwfilter
63	lastrttyfilter
64	lastpktfilter
65	lastclariferstate
66	skipscanamfilter
67	amfm100
*/

