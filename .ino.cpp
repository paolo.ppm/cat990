#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-03-25 23:11:26

#include "Arduino.h"
#include <Arduino.h>
#include <CustomSoftwareSerial.h>
#include "Cat990.h"
void setup();
void ParseCmdFromPC() ;
void SendCmd() ;
void AnswMeter() ;
void RadioFlags() ;
void GetFlags() ;
void AnswFlags() ;
void StepFrequency(unsigned char stepSign) ;
uint64_t bin2bcd(uint32_t usint) ;
uint32_t bcd2bin(uint64_t bcd) ;
void GetFullUpdate() ;
void ByPassCmd() ;
void DecodeMem(int ndx) ;
void AnswMemory(unsigned char param) ;
void SendUpdArray(int first, int num) ;
void AnswUpdate(unsigned char param1, unsigned char param2) ;
int GetSerialTimeout() ;
void GetFirstUpdate() ;
void FlashLed13() ;
void loop() ;

#include "CAT_990.ino"


#endif
